    <?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<div class="bc_breadcrumbs"><ul>';
foreach($arResult as $arItem):
    $strReturn .='<li><a href="'.$arItem["LINK"].'">'.$arItem["TITLE"].'</a></li>';
endforeach;

$strReturn .= '</ul><div class="clearboth"></div></div>';

return $strReturn;
