<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <div class="sb_nav">
    <ul>
    <?
    $previousLevel = 1;
    foreach($arResult as $arItem):
    ?>
        <!-- Открытие списка -->
        <?while ($arItem["DEPTH_LEVEL"] < $previousLevel):?>
            </ul>
            </li>
            <?$previousLevel -= 1;?>
        <?endwhile;?>

        <!-- Закрытие списков -->
        <?while ($arItem["DEPTH_LEVEL"] > $previousLevel):?>
            <ul>
            <?$previousLevel += 1;?>
        <?endwhile;?>

        <?if ($arItem["DEPTH_LEVEL"] == 1):?>
            <li class= "<?=($arItem["SELECTED"] ? "open current" : "close" )?>" >
                <?if ($arItem["IS_PARENT"]):?>
                    <span class="sb_showchild"></span>
                <?endif;?>
                <a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
        <?else:?>
            <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
        <?endif;?>

        <?if (!$arItem["IS_PARENT"]):?>
            </li>
        <?endif;?>
    <?endforeach;?>

    <!-- Закрытие списков -->
    <?while ($previousLevel > 1):?>
        </ul>
        </li>
        <?$previousLevel -= 1;?>
    <?endwhile;?>

    <div class="clearboth"></div>
    </ul>
    </div>
<?endif;?>