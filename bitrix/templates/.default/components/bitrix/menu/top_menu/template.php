<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <div class="nv_topnav">
        <ul>
            <?
            $previousLevel = 1;
            foreach($arResult as $arItem):
            ?>
                <?while ($arItem["DEPTH_LEVEL"] < $previousLevel):?>
                    </ul>
                    </li>
                    <?$previousLevel -= 1;?>
                <?endwhile;?>

                <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <?if (!$arItem["IS_PARENT"] && empty($arItem["TEXT"])):?>
                        <li><a href="<?=$arItem["LINK"]?>"   class="menu-img-fon"  style="background-image: url(/bitrix/templates/.default/images/nv_home.png);" ><span></span></a></li>
                    <?else:?>
                        <li><a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
                    <?endif;?>
                <?else:?>
                    <?while ($arItem["DEPTH_LEVEL"] > $previousLevel):?>
                        <ul>
                        <?$previousLevel += 1;?>
                    <?endwhile;?>
                    <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                <?endif;?>
                <?if (!$arItem["IS_PARENT"]):?>
                    </li>
                <?endif;?>
            <?endforeach;?>

            <?while ($previousLevel > 1):?>
                </ul>
                </li>
                <?$previousLevel -= 1;?>
            <?endwhile;?>

            <div class="clearboth"></div>
        </ul>
    </div>
<?endif;?>