<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$APPLICATION->SetPageProperty("title",$arResult["PROPERTIES"]["BROWSER_TITLE"]["VALUE"]);
$APPLICATION->SetPageProperty("keywords",$arResult["PROPERTIES"]["KEYWORDS"]["VALUE"]);
$APPLICATION->SetPageProperty("description",$arResult["DETAIL_TEXT"]);
?>

<div class="ps_head"><span class="ps_date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span></div>
<?if(is_array($arResult["DETAIL_PICTURE"])):?>
<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" align="left" alt="<?=$arResult["NAME"]?>"/> 
<?endif;?>
<?=$arResult["DETAIL_TEXT"];?>
<?if (!empty($APPLICATION->GetProperty("keywords"))):?>
    <p>Ключевые слова: <?=$APPLICATION->GetProperty("keywords");?></p>
<?endif;?>
<?if (!empty($APPLICATION->GetProperty("description"))):?>
    <p>Описание: <?=$APPLICATION->GetProperty("description");?></p>
<?endif;?>
