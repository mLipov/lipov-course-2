<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



<?$arItem = array_shift($arResult["ITEMS"]);?>

<div class="sb_reviewed">
    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="sb_rw_avatar" alt=""/>
    <span class="sb_rw_name"><?=$arItem["NAME"]?></span>
    <span class="sb_rw_job"><?=$arItem["PROPERTIES"]["post"]["VALUE"]?> “<?=$arItem["PROPERTIES"]["company_name"]["VALUE"]?>”</span>
    <p>“<?=$arItem["DETAIL_TEXT"]?>”</p>
    <div class="clearboth"></div>
    <div class="sb_rw_arrow"></div>
</div>

