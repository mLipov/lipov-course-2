<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?$arItem = array_shift($arResult["ITEMS"]);?>

<div class="sb_action">
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt=""/></a>
    <h4><?=GetMessage("PROMOTION");?></h4>
    <h5><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["PREVIEW_TEXT"];?></a></h5>
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="sb_action_more"><?=GetMessage("MORE");?></a>
</div>
