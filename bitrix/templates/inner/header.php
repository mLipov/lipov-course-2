<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE HTML>
<html lang="ru">
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>

    <?
    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/jquery-1.8.2.min.js");
    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/slides.min.jquery.js");
    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/functions.js");
    ?>
    <!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->

    <?$APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/template_styles.css");?>

    <link rel="SHORTCUT ICON" href="/bitrix/templates/.default/favicon.ico" type="image/x-icon">
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<div class="wrap">
    <?include_once ($_SERVER['DOCUMENT_ROOT']."/bitrix/templates/.default/include/header_nav_menu.php");?>

    <!--- Нав. цепочка   --->
    <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        "nav",
        Array(
            "PATH" => "",
            "SITE_ID" => "s1",
            "START_FROM" => "0"
        )
    );?>
    <div class="main_container page">
        <div class="mn_container">
            <div class="mn_content">
                <div class="main_post">
                    <div class="main_title">
                        <p class="title"><?$APPLICATION->ShowTitle(false);?></p>
                    </div>


